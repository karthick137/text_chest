import java.util.ArrayList;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.File;

import java.util.zip.GZIPOutputStream;
import java.util.zip.GZIPInputStream;

class CompressedList{
	private byte buffer[];
		ByteArrayOutputStream baos;
		GZIPOutputStream gzipOut;
		ObjectOutputStream objectOut;
	CompressedList(){
		init();
	}
	void init(){
		try{
			baos = new ByteArrayOutputStream();
			gzipOut = new GZIPOutputStream(baos);
			objectOut = new ObjectOutputStream(gzipOut);
		}
		catch(IOException exc){
			System.out.println(exc.getMessage());
		}
	}
	void add(String input){
		try{
			objectOut.writeObject(input);
			buffer = baos.toByteArray();
		}
		catch(IOException exc){
			System.out.println(exc.getMessage());
		}
	}
	String read(){
		try{
			objectOut.close();
			buffer = baos.toByteArray();
			ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
			GZIPInputStream gzipIn = new GZIPInputStream(bais);
			ObjectInputStream objectIn = new ObjectInputStream(gzipIn);
			init();
			return (String)objectIn.readObject();
		}
		catch(IOException|ClassNotFoundException exc){
			System.out.println(exc.getMessage());
			exc.printStackTrace();
		}
		return null;

	}
	void printAll(){
		try{
			objectOut.close();
			buffer = baos.toByteArray();
			ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
			GZIPInputStream gzipIn = new GZIPInputStream(bais);
			ObjectInputStream objectIn = new ObjectInputStream(gzipIn);
			String line;
			int counter = 0;
			while((line = (String)objectIn.readObject()) != null){
				System.out.println(counter);
				if(line != null)
				System.out.println(line);
				System.gc();
				counter++;
			}
			init();
		}
		catch(IOException|ClassNotFoundException exc){
			System.out.println(exc.getMessage());
		}

	}
	int size(){
		return buffer.length/1024;
	}
}
public class TextChest{
	static int write(int level){
		if(level == 5){
			return 0 ;
		}
		String alphabet = "abcdefghijklmnopqrstuvwxyz";
		try{
			boolean appendReversedAlphabet = true;
			for(int counter = 0; counter < 100; counter++){
				alphabet += appendReversedAlphabet ? new StringBuilder(alphabet).reverse().toString() : alphabet;
				appendReversedAlphabet = !appendReversedAlphabet;	
				String current = alphabet + System.getProperty("line.separator");

				File temp = new File("output.txt");
				if(temp.exists() && !temp.isDirectory())
					Files.write(Paths.get("output.txt"), current.getBytes(), StandardOpenOption.APPEND);
				else
					Files.write(Paths.get("output.txt"), current.getBytes());


			}
		}
		catch(OutOfMemoryError exc){
			alphabet = null;
			System.gc();
			level++;
			return write(level++);
		}
		catch(IOException exc){
			System.out.println(exc.getMessage());
		}
		return 0;
	}

	static CompressedList read(){
		CompressedList list = new CompressedList();
		try(BufferedReader reader = new BufferedReader(new FileReader("output.txt"))){
			String line;
			while((line = reader.readLine()) != null){
				line = line.intern();
				list.add(line);	
				System.gc();
			}	
			list.printAll();
			System.out.printf("Size :%d", list.size());
		}
		catch(IOException exc){
			System.out.println(exc.getMessage());
		}
		return list;
	}
	public static void main(String[] args){
		write(1);	
		CompressedList list = read();
		System.gc();
	}
}
